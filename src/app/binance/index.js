import api from 'binance';
import { ValidationError } from '../utils/error_handler';

const binanceWS = new api.BinanceWS(true);
const { BINANCE_KEY, BINANCE_SECRET } = process.env;

export async function getExchangeSymbols(key = BINANCE_KEY, secret = BINANCE_SECRET) {
  const binanceRest = new api.BinanceRest({ key, secret });
  const data = await binanceRest.exchangeInfo();
  return data;
}

export async function account(key, secret) {
  try {
    const binanceRest = new api.BinanceRest({ key, secret });
    const data = await binanceRest.account();
    return data;
  } catch (error) {
    throw new ValidationError('Invalid account key & secret');
  }
}

export async function trade(key, secret, symbol) {
  let binanceRest;
  try {
    binanceRest = new api.BinanceRest({ key, secret });
  } catch (error) {
    throw new ValidationError('Invalid account key & secret');
  }
  try {
    const data = await binanceRest.trades({ symbol });
    return data;
  } catch (error) {
    throw new ValidationError('Invalid Symbol');
  }
}

export function alertPull(symbol) {
  return binanceWS.onAggTrade(symbol, () => {});
}
