import EventEmitter from 'events';
import api from 'binance';
import * as redis from '../utils/redis';

const eventEmitter = new EventEmitter();
const binanceWS = new api.BinanceWS(true);

const reached = (arr, symbol, action, price) => {
  arr.map((e) => JSON.parse(e)).forEach((e) => {
    global.bot.telegram.sendMessage(e.chatId, `Alert: ${e.token}\nPair: ${symbol}\nAction: reached ${action} value\nCurrent Price: ${price}`);
  });
};

eventEmitter.on('newPair', (symbol) => {
  let price;
  setTimeout(() => {
    binanceWS.onAggTrade(symbol, (data) => {
      price = data.price;
    });
  }, 0);

  setInterval(async () => {
    const max = await redis.getMaxSetByRange(symbol, price);
    const min = await redis.getMinSetByRange(symbol, price);
    reached(min, symbol, 'min', price);
    reached(max, symbol, 'max', price);
  }, process.env.ALERT_INTERVAL);
});

export default eventEmitter;
