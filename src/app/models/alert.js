module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('alert', {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
    },
    symbol: DataTypes.STRING,
    max: DataTypes.INTEGER,
    min: DataTypes.INTEGER,
    enabled: DataTypes.BOOLEAN,
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.fn('now'),
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.fn('now'),
    },
  }, {
    tableName: 'alerts',
    underscored: true,
    paranoid: true,
    defaultScope: {
      attributes: { exclude: ['updatedAt', 'createdAt', 'deletedAt'] },
    },
    hooks: {
      afterSave: async (n) => {
        // eslint-disable-next-line no-param-reassign
        if (n && n.dataValues) ['createdAt', 'updatedAt', 'deletedAt'].forEach((prop) => delete n.dataValues[prop]);
      },
    },
  });
  model.associate = function associate(models) {
    models.alert.belongsTo(models.user);
  };
  return model;
};
