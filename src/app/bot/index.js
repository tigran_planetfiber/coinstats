/* eslint-disable no-restricted-syntax */
import path from 'path';
import fs from 'fs';
import { Telegraf } from 'telegraf';

const bot = new Telegraf(process.env.TELEGRAM_BOT_KEY);

const ApiFolder = path.join(__dirname, '..', 'api');
const asyncIterable = {
  [Symbol.asyncIterator]() {
    return {
      directories: fs.readdirSync(ApiFolder, { withFileTypes: true })
        .filter((dirent) => dirent.isDirectory()),
      next() {
        if (this.directories.length) {
          return Promise.resolve({ value: this.directories.pop(), done: false });
        }
        return Promise.resolve({ done: true });
      },
    };
  },
};

export default async function createBot() {
  for await (const { name: resource } of asyncIterable) {
    const resBot = await import(path.join(ApiFolder, resource, 'bot.js'));
    resBot.default.forEach((resBotCommand) => {
      bot.command(resBotCommand.command, resBotCommand.callback);
    });
  }
  bot.startPolling();
  return bot;
}
