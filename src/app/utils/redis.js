import { print } from 'redis';

export function get(key) {
  return new Promise((resolve, reject) => {
    global.redisClient.get(key, (error, response) => {
      if (error) reject(error);
      const parsedResponse = response ?? JSON.parse(response);
      resolve(parsedResponse);
    });
  });
}

export function set(key, value) {
  return new Promise((resolve, reject) => {
    const stringifyValue = JSON.stringify(value);
    global.redisClient.set(key, stringifyValue, (error, response) => {
      if (error) reject(error);
      resolve(response);
    });
  });
}

export function del(key) {
  return new Promise((resolve, reject) => {
    global.redisClient.del(key, (error, response) => {
      if (error) reject(error);
      resolve(response);
    });
  });
}

export function keys(key) {
  return new Promise((resolve, reject) => {
    global.redisClient.keys(key, (error, response) => {
      if (error) reject(error);
      resolve(response);
    });
  });
}

export function mGet(key) {
  return new Promise((resolve, reject) => {
    global.redisClient.mget(key, (error, response) => {
      if (error) reject(error);
      resolve(response);
    });
  });
}

export function isSetMember(key, value) {
  return new Promise((resolve, reject) => {
    global.redisClient.sismember(key, value, (error, response) => {
      if (error) reject(error);
      resolve(response);
    });
  });
}

export function getSetMembers(key) {
  return new Promise((resolve, reject) => {
    global.redisClient.smembers(key, (error, response) => {
      if (error) reject(error);
      const parsedResponse = response ?? JSON.parse(response);
      resolve(parsedResponse);
    });
  });
}

export function getMaxSetByRange(key, value) {
  return new Promise((resolve, reject) => {
    global.redisClient.zrangebyscore(`${key}-MAX`, '-inf', value, (error, response) => {
      if (error) reject(error);
      const parsedResponse = response ?? JSON.parse(response);
      global.redisClient.zremrangebyscore(`${key}-MAX`, '-inf', value);
      resolve(parsedResponse);
    });
  });
}

export function getMinSetByRange(key, value) {
  return new Promise((resolve, reject) => {
    global.redisClient.zrangebyscore(`${key}-MIN`, value, '+inf', (error, response) => {
      if (error) reject(error);
      const parsedResponse = response ?? JSON.parse(response);
      global.redisClient.zremrangebyscore(`${key}-MIN`, value, '+inf');
      resolve(parsedResponse);
    });
  });
}

export function client() {
  return global.redisClient;
}

export function printFn() {
  return print;
}
