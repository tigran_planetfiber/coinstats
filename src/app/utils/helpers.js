/* eslint-disable no-plusplus */

export default function insertInSortedArrayOfObject(arr, value, keyInObject) {
  for (let i = 0; i < arr.length; i++) {
    if (value[keyInObject] < arr[i][keyInObject]) {
      arr.splice(i, 0, value);
      break;
    }
  }
  return arr;
}
