import coreService from '../../core/service';
import repository from './repository';
import { ServiceError, ValidationError } from '../../utils/error_handler';
import * as binance from '../../binance';

const service = Object.create(coreService);

export default Object.assign(service, {

  /**
   *  Get One available Trade Pairs
   *  @param {UUID} token : user ID
   *  @param {Boolean} asBuffer : return value as buffer or json
   *  @return {Object} buffer or json
   */
  async getPairs(token, asBuffer = false) {
    try {
      const user = await repository.findByPk(token);
      const data = await binance.getExchangeSymbols(user.key, user.secret);
      return asBuffer ? Buffer.from(data.symbols.map((i) => i.symbol).join('\n')) : data;
    } catch (error) {
      throw new ValidationError('Invalid Token');
    }
  },

  /**
   *  Create resources
   *  @param {Object} data : resource data object
   *  @return {Object} resource ORM object
   */
  async store(data) {
    try {
      await binance.account(data.key, data.secret);
      const { id } = await repository.create({
        enabled: true,
        ...data,
      });
      return id;
    } catch (error) {
      throw new ServiceError(error);
    }
  },

  /**
   *  Remove resources
   *  @param {UUID} id : user ID
   *  @return {Object} empty object
   */
  async remove(id) {
    try {
      await repository.destroy({ id });
      return {};
    } catch (error) {
      throw new ValidationError('Invalid Token');
    }
  },
});
