import * as controller from './controller';
import validator from './validator';

export default (router) => {
  router.get('/init', validator.store, controller.store);
  router.get('/getAllTradingPairs', validator.store, controller.getPairs);
  router.delete('/destroy', validator.isExist, controller.remove);
};
