import service from './service';

export default [
  {
    command: '/init',
    callback: async (ctx) => {
      try {
        const [, key, secret] = ctx.update.message.text.split(' ');
        let id;
        if (key && secret) {
          id = await service.store({ key, secret, chatId: ctx.update.message.from.id });
        } else {
          throw new Error('Please input key and secret:\n/init {key} {secret}');
        }
        return ctx.reply(`API key: ${key}\nAPI secret: ${secret}\n\nToken: ${id}`);
      } catch (error) {
        return ctx.reply(error.message);
      }
    },
  },
  {
    command: '/getAllTradingPairs',
    callback: async (ctx) => {
      try {
        const [, token] = ctx.update.message.text.split(' ');
        const pairs = await service.getPairs(token, true);
        return ctx.replyWithDocument({ source: pairs, filename: 'test.csv' });
      } catch (error) {
        return ctx.reply(error.message);
      }
    },
  },
  {
    command: '/destroy',
    callback: async (ctx) => {
      try {
        const [, token] = ctx.update.message.text.split(' ');
        await service.remove(token);
        return ctx.reply('destroyed');
      } catch (error) {
        return ctx.reply(error.message);
      }
    },
  },
];
