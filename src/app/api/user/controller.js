import service from './service';
import cw from '../../core/controller';

export const store = cw((req) => {
  req.definedStatus = 201;
  return service.store(req.query);
});

export const getPairs = cw((req) => service.getPairs(req.query.token));

export const remove = cw((req) => service.remove(req.query.token));
