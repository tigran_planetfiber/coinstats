/* eslint-disable quote-props */
import coreService from '../../core/service';
import repository from './repository';
import { ValidationError } from '../../utils/error_handler';
import * as redis from '../../utils/redis';
import * as binance from '../../binance';
import eventEmitter from '../../binance/events';

const service = Object.create(coreService);

export default Object.assign(service, {
  /**
   *  Create resources
   *  @param {Object} data : resource data object
   *  @return {Object} resource ORM object
   */
  async store(data) {
    try {
      const {
        id: userId,
        key,
        secret,
        chatId,
      } = await repository.models.user.findByPk(data.token);

      if (!userId) {
        throw new ValidationError('Invalid token');
      }

      const symbol = data.symbol.replace('/', '').trim().toUpperCase();
      const min = +data.min;
      const max = +data.max;

      await binance.trade(key, secret, symbol);

      const { id } = await repository.create({
        userId,
        symbol,
        min,
        max,
      });

      const redisClient = redis.client();

      const before = await redis.isSetMember('pairs', symbol);

      if (!before) {
        redisClient.sadd('pairs', symbol);
        eventEmitter.emit('newPair', symbol);
      }

      redisClient.zadd(`${symbol}-MAX`, data.max, JSON.stringify({ chatId, token: id }));
      redisClient.zadd(`${symbol}-MIN`, data.min, JSON.stringify({ chatId, token: id }));

      return id;
    } catch (error) {
      throw new ValidationError(error);
    }
  },

  clients: [],
  async getPull(req, res) {
    try {
      const headers = {
        'Content-Type': 'text/event-stream',
        'Connection': 'keep-alive',
        'Cache-Control': 'no-cache',
      };
      res.writeHead(200, headers);

      const { symbol } = await repository.models.alert.findByPk(req.query.token);
      binance.alertPull(symbol).on('message', (data) => {
        res.write(data);
      });

      const clientId = Date.now();

      const newClient = {
        id: clientId,
        res,
      };

      this.clients.push(newClient);

      req.on('close', () => {
        this.clients = this.clients.filter((client) => client.id !== clientId);
      });
    } catch (error) {
      throw new ValidationError(error);
    }
  },
});
