import service from './service';
import cw from '../../core/controller';

export const get = (req, res) => service.getPull(req, res);

export const store = cw((req) => service.store(req.body));
