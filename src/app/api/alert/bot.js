import service from './service';

export default [
  {
    command: '/createAlert',
    callback: async (ctx) => {
      try {
        const [, token, symbol, min, max] = ctx.update.message.text.split(' ');
        const alertToken = await service.store({
          token, symbol, min, max,
        });
        return ctx.reply(`Alert Token: ${alertToken}`);
      } catch (error) {
        return ctx.reply(error.message);
      }
    },
  },
];
