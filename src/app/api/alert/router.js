import * as controller from './controller';
import validator from './validator';

export default (router) => {
  router.post('/createAlert', validator.store, controller.store);
  router.get('/alertPull', validator.isExist, controller.get);
};
