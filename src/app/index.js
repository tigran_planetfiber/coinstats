import * as express from 'express';
import path from 'path';
import fs from 'fs';
import async from 'async';

const ApiFolder = path.join(__dirname, 'api');
const ApiPaths = [];

const getDirectories = (source) => fs.readdirSync(source, { withFileTypes: true })
  .filter((dirent) => dirent.isDirectory());

const getFiles = (source) => fs.readdirSync(source, { withFileTypes: true })
  .filter((dirent) => dirent.isFile());

const createRoutes = (prefix, source) => {
  getDirectories(source)
    .map((folder) => {
      const files = getFiles(path.join(source, folder.name));
      if (!files[0]) {
        return createRoutes(path.join(prefix, folder.name), path.join(source, folder.name));
      }
      return ApiPaths.push(path.join(prefix, folder.name));
    });
};

export default async function Router(router) {
  const Modules = [];
  createRoutes('', ApiFolder);
  await async.forEach(ApiPaths, async (api) => {
    const route = await import(path.join(ApiFolder, api, 'router.js'));

    function Module(apiRouter) {
      this.router = express.Router();
      this.apiRouter = apiRouter;

      this.createEndpoints = () => {
        // this.apiRouter.use(`/${api}`, this.router);
        this.apiRouter.use('/', this.router);
        route.default(this.router);
      };
    }
    Modules.push((apiRouter) => new Module(apiRouter));
  });
  Modules.forEach((module) => module(router).createEndpoints());
  return router;
}
