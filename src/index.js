import http from 'http';
import app from './app';

process.env.NODE_ENV = (process.env.NODE_ENV || 'development');

const server = http.createServer(app());
server.listen(process.env.PORT, process.env.HOST, () => {
  // eslint-disable-next-line no-console
  console.log('API listening at http://%s:%s', server.address().address, server.address().port, ' pid: ', process.pid);
});
