#!/usr/bin/env node
const prompts = require('prompts');
const figlet = require('figlet');
const fs = require('fs');
const path = require('path');

prompts.override(require('yargs').argv);

console.clear();

const api = path.join(__dirname, '..', '..', 'app', 'api');

const getDirectories = (source) => fs.readdirSync(source, { withFileTypes: true })
  .filter((dirent) => dirent.isDirectory());

const getFiles = (source) => fs.readdirSync(source, { withFileTypes: true })
  .filter((dirent) => dirent.isFile());

const apiChoices = [
  {
    title: 'Api // no subdirectory',
    value: 'api',
  },
];
  
getDirectories(api)
  .map((folder) => {
  const files = getFiles(path.join(api, folder.name));
  return !files[0] && apiChoices.push({
    title: folder.name,
    value: folder.name,
  });
});

async function runPrompts() {
  const response = await prompts([
    {
      type: 'select',
      name: 'action',
      message: 'What you want to do?',
      choices: [
        { title: 'Run app', value: 'run' },
        { title: 'Make something', value: 'make' },
        { title: 'Help me', value: 'help' },
      ],
    },
    {
      type: (prev) => (prev === 'make' ? 'select' : null),
      name: 'make',
      message: 'What you want to create?',
      choices: [
        { title: 'Module', value: 'module' },
        { title: 'DB model', value: 'model' },
      ],
    },
    {
      type: (prev, values) => (values.make === 'module' ? 'text' : null),
      name: 'moduleName',
      message: 'What\'s your module name?',
    },
    {
      type: 'select',
      name: 'moduleDirectory',
      message: 'Which api subdirectory are you going to use?',
      choices: apiChoices,
    },
    {
      type: (prev, values) => (values.make === 'module' ? 'select' : null),
      name: 'model',
      message: 'Do you want to create DB model for this module?',
      choices: [
        { title: 'Yes, creat DB model', value: 1 },
        { title: 'No, create without model', value: 0 },
      ],
    },
    {
      type: (prev, values) => (values.model ? 'text' : null),
      name: 'modelName',
      message: 'What\'s your model name?',
      initial: (prev, values) => values.moduleName,
    },
    {
      type: (prev, values) => (values.model ? 'text' : null),
      name: 'tableName',
      message: 'What\'s your table name?',
      initial: (prev, values) => `${values.moduleName}s`,
    },
  ]);

  if (
    (response.action === 'make' && 
    response.make === 'module' && 
    response.moduleName[0] && 
    response.moduleDirectory[0]) || 
    response.action === 'run') {
    require('./make').makeModel(response);
  }
}

figlet('CoinStats  CLI', (err, data) => {
  if (err) {
    console.log('Something went wrong...');
    console.dir(err);
    return;
  }
  console.log(data);
  runPrompts();
});
