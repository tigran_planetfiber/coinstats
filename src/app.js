/* eslint-disable no-console */
/* eslint-disable class-methods-use-this */
import fs from 'fs';
import path from 'path';
import express from 'express';
import bodyParser from 'body-parser';
import expressValidator from 'express-validator';
import logger from 'morgan';
import Sequelize from 'sequelize';
import redis from 'redis';
import fileUpload from 'express-fileupload';
import sequelizeConfig from '../sequelizeConfig';
import enableRoutes from './app/';
import { setUpDb } from './app/models';
import { ErrorHandler } from './app/utils/error_handler';
import { getSetMembers } from './app/utils/redis';
import initBot from './app/bot';
import eventEmitter from './app/binance/events';

class Application {
  constructor() {
    this.app = express();
    this.initApp();
  }

  async initApp() {
    await this.configApp();
    await this.configLogger();
    await this.dbConfig();
    await this.configRedis();
    await this.setParams();
    await this.setFileParser();
    await this.setRouter();
    await this.setErrorHandler();
    await this.set404Handler();
    await this.startBot();
    await this.startActiveEvents();
  }

  async configApp() {
    this.app.use(bodyParser.json({
      limit: '100mb',
    }));
    this.app.use(bodyParser.urlencoded({
      limit: '100mb',
      extended: true,
    }));
    this.app.use(expressValidator());
    this.app.use((req, res, next) => {
      res.setHeader('Access-Control-Allow-Origin', '*');
      res.setHeader('Access-Control-Allow-Credentials', true);
      res.setHeader('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE');
      res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
      if (req.method === 'OPTIONS') {
        return res.json();
      }
      return next();
    });
    if (process.NODE_ENV !== 'production') {
      this.app.use('/api/assets', express.static(path.join(__dirname, '../', 'public')));
    }
  }

  async configLogger() {
    const logFilePath = path.join(__dirname, '../', 'access.log');
    const accessLogStream = fs.createWriteStream(logFilePath, { flags: 'a' });
    this.app.use(logger('combined', { stream: accessLogStream }));
    this.app.use('/logger', (req, res) => res.sendFile(logFilePath));
  }

  async dbConfig() {
    const sequelize = new Sequelize(sequelizeConfig);
    this.db = await setUpDb(sequelize);
  }

  async configRedis() {
    const client = await redis.createClient({
      port: process.env.REDIS_PORT,
      host: process.env.REDIS_HOST,
    });
    client.on('error', (error) => {
      console.log(`Error REDIS ${error}`);
    });
    client.on('connect', () => {
      console.log('REDIS connection ready');
    });
    global.redisClient = client;
  }

  async setParams() {
    this.app.set('env', process.env.NODE_ENV);

    // eslint-disable-next-line no-extend-native
    BigInt.prototype.toJSON = function toJSON() {
      return this.toString();
    };
  }

  async setFileParser() {
    global.uploadFolder = path.join(__dirname, '..', process.env.UPLOAD_FOLDER);
    if (!fs.existsSync(global.uploadFolder)) {
      fs.mkdirSync(global.uploadFolder);
    }
    this.app.use(fileUpload({ safeFileNames: /\\/g, preserveExtension: 0, createParentPath: true }));
  }

  async setRouter() {
    this.router = express.Router();
    this.app.use('/api', await enableRoutes(this.router));
  }

  async set404Handler() {
    this.app.use((_req, res) => {
      res.status(404).json({
        status: 'Error',
        message: '',
        data: null,
        errors: '',
      });
    });
  }

  async startBot() {
    try {
      global.bot = await initBot();
      console.log('BOT successfully Started');
    } catch (error) {
      throw new Error(error);
    }
  }

  async startActiveEvents() {
    try {
      const pairs = await getSetMembers('pairs');
      pairs.forEach((pair) => {
        eventEmitter.emit('newPair', pair);
      });
    } catch (error) {
      throw new Error(error);
    }
  }

  async setErrorHandler() {
    // eslint-disable-next-line no-unused-vars
    this.app.use((error, _req, res, _next) => {
      if (error.formatWith && typeof error.formatWith === 'function') {
        return res.status(422).json({
          status: 'Error',
          message: error.message,
          data: null,
          errors: error.mapped(),
        });
      }
      // TODO: log in error_log file
      console.error(error.stack);
      if (error instanceof ErrorHandler) {
        return res.status(error.status || 400).json({
          status: error.name,
          message: error.message,
          data: null,
          errors: error.errors,
        });
      }
      // TODO: send log errors to server
      return res.status(500).json({
        status: 'Error',
        message: error.message,
        data: null,
        errors: null,
      });
    });
  }
}

const instanceOfApplication = new Application();
export default () => instanceOfApplication.app;
